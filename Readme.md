
## To initialize the env

    nvm use v14.18.1
    yarn install
    yarn docs:dev

## To update the weekly reports

    export NL_GRAFANA_API_KEY="eyJr...."
    bash -x scripts/update-graphs.sh

## Screenshots

![Screenshot](./Screenshot_2023-01-16_15-04-43.png)
