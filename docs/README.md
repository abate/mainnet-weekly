---
lang: en-US
title: Weekly Reports
description: Mainnet Health report
---

# This Week on Mainnet

## Head Operations

![Head Operations](./images/latest/image-12.png)

## Head Level History

![Head Level](./images/latest/image-13.png)

## Block Validation Time

![Block Validation Time](./images/latest/image-14.png)

## Gas Consumed History

![Gas Consumed History](./images/latest/image-15.png)

### Past Weeks
[Week 03](./week-03-2023.md)
[Week 03](./week-03-2023.md)
[Week 03](./week-03-2023.md)
[Week 03](./week-03-2023.md)
