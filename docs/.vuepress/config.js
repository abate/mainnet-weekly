import { defineUserConfig } from 'vuepress'

export default defineUserConfig({
  lang: 'en-US',
  title: 'Mainnet Healty Weekly Reports',
  base: '/mainnet-weekly/',
  dest: 'public'
})
