#!/bin/bash

# https://grafana.obs.nomadic-labs.cloud/render/d-solo/2YL6zfPnz/tezos-node?orgId=1&from=1673603025708&to=1673624625708&panelId=11&width=1000&height=500&tz=Europe%2FParis
# Server configuration
URL=grafana.obs.nomadic-labs.cloud
ORG_ID=1

DASHBOARD_NAME="grafazos-compact-dashboard"
DASHBOARD_ID="-Wda_s3nk"
PANELS_ID=(12 13 14 15)

fetch_image()
  {
    PANEL_ID=$1

    WIDTH=800
    HEIGHT=600
    TIMEZONE=CET

curl -k -H "Authorization: Bearer ${NL_GRAFANA_API_KEY}" "https://${URL}/render/d-solo/${DASHBOARD_ID}/${DASHBOARD_NAME}?orgId=${ORG_ID}from=now-7d&panelId=${PANEL_ID}&width=${WIDTH}&height=${HEIGHT}&tz=${TIMEZONE}" > "docs/images/${WEEKNUMBER}/image-${PANEL_ID}.png"
  }

WEEKNUMBER=$(date +%U)
mkdir -p "docs/images/${WEEKNUMBER}"
rm -f docs/images/latest
( cd docs/images && ln -s "${WEEKNUMBER}/" latest )

for PID in ${PANELS_ID[*]}; do
  fetch_image "$PID"
done

cat <<EOF > "docs/week-${WEEKNUMBER}-2023.md"

# Week ${WEEKNUMBER} on Mainnet

## Head Operations

![Head Operations](./images/${WEEKNUMBER}/image-12.png)

## Head Level History

![Head Level](./images/${WEEKNUMBER}/image-13.png)

## Block Validation Time

![Block Validation Time](./images/${WEEKNUMBER}/image-14.png)

## Gas Consumed History

![Gas Consumed History](./images/${WEEKNUMBER}/image-15.png)

EOF

echo "[Week ${WEEKNUMBER}](./week-${WEEKNUMBER}-2023.md)" >> docs/README.md
